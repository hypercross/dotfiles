" ================= nvim env
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
" ================= cursor
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"
set ttimeoutlen=10
" ================= editor commons
set bkc=yes
set encoding=utf-8
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4 et
set laststatus=2 showcmd
set nu cursorline
set incsearch nohlsearch
set t_Co=256
set shell=bash
set autoindent
syntax on
"set completeopt=longest

" ================= vim-plug
call plug#begin('~/.vim/bundle')

" ================= tools & integration
Plug 'Chiel92/vim-autoformat'

Plug 'benekastah/neomake'
let g:neomake_javascript_enabled_makers = ["eslint"]
autocmd! BufWritePost * Neomake

"Plug 'Shougo/deoplete.nvim'
"let g:deoplete#enable_at_startup = 1

Plug 'Valloric/YouCompleteMe'
"nmap <F7> :YcmCompleter GoToDefinition<CR>
"nmap K :YcmCompleter GetType<CR>
"
"Plug 'majutsushi/tagbar'

" ================= visuals
Plug 'nathanaelkane/vim-indent-guides'
set background=dark

Plug 'airblade/vim-gitgutter'
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

Plug 'junegunn/goyo.vim'

Plug 'junegunn/limelight.vim'

Plug 'bling/vim-airline'
  let g:airline_powerline_fonts=1

  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

  " powerline symbols
  let g:airline_left_sep = ''
  let g:airline_left_alt_sep = ''
  let g:airline_right_sep = ''
  let g:airline_right_alt_sep = ''
  let g:airline_symbols.branch = ''
  let g:airline_symbols.readonly = ''
  let g:airline_symbols.linenr = ''
  let g:airline#extensions#ctrlspace#enabled = 1

Plug 'morhetz/gruvbox'

if !$NVIM_TUI_ENABLE_TRUE_COLOR
    Plug 'edkolev/tmuxline.vim'
endif

" ================= tweaks & shortcuts
Plug 'Konfekt/FastFold'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'Raimondi/delimitMate'
Plug 'guns/vim-sexp'
Plug 'tpope/vim-sexp-mappings-for-regular-people'
Plug 'michaeljsmith/vim-indent-object'

"Plug 'AutoComplPop'
"Plug 'othree/vim-autocomplpop'

" ================= utilities

Plug 'SirVer/ultisnips'
let g:UltiSnipsSnippetDirectories=["UltiSnips","hyperSnips"]
let g:UltiSnipsSnippetsDir = "~/.vim/hyperSnips"
let g:UltiSnipsExpandTrigger = "<nop>"
inoremap <expr> <CR> pumvisible() ? "<C-R>=UltiSnips#ExpandSnippetOrJump()<CR>" : "\<CR>"

Plug 'honza/vim-snippets'

Plug 'scrooloose/nerdtree' " nerdtree explorer

Plug 'scrooloose/nerdcommenter'

Plug 'easymotion/vim-easymotion'
let g:EasyMotion_do_shade = 0

Plug 'junegunn/fzf.vim'
set rtp+=~/.fzf
autocmd VimEnter * command! Colors
            \ call fzf#vim#colors({'left': '15%', 'options': '--reverse --margin 30%,0'})

Plug 'Shougo/unite.vim'
let g:unite_enable_auto_select = 0
let g:unite_source_rec_async_command =
	\ ['ag', '--follow', '--nocolor', '--nogroup',
	\  '--hidden', '-g', '']

Plug 'Shougo/vimproc.vim'

Plug 'Shougo/unite-outline'

Plug 'tpope/vim-fugitive'

Plug 'mhinz/vim-grepper'

"Plug 'rking/ag.vim'

" =============== language specific

Plug 'dag/vim-fish'
Plug 'evanmiller/nginx-vim-syntax'
Plug 'kchmck/vim-coffee-script'
Plug 'mattn/emmet-vim'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'leafgarland/typescript-vim'
Plug 'tpope/vim-fireplace'
Plug 'mustache/vim-mustache-handlebars'

"Plug 'derekwyatt/vim-scala'
"Plug 'vim-ruby/vim-ruby'
"Plug 'elixir-lang/vim-elixir'
"Plug 'Quramy/tsuquyomi'
"let g:tsuquyomi_disable_quickfix = 1
"Plug 'ternjs/tern_for_vim'
"au FileType javascript nmap <F7> :TernDef<CR>
"au FileType javascript nmap <C-F7> :TernRefs<CR>
"au FileType javascript nmap <S-K> :TernDoc<CR>

" ================= end of vim-plug
call plug#end()

" ================= start config
colorscheme Tomorrow-Night-Eighties
set foldmethod=indent foldlevel=10
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 1
au FileType javascript set tabstop=2 shiftwidth=2
au FileType javascript nmap <F3> :silent !eslint % --fix<CR>:Neomake<CR>
au FileType Clojure 
au BufWritePost *.clj silent Require

" ================= unite and fzf
nmap <space>c :Unite change -prompt=> -auto-resize<CR>
nmap <space>C :Colors<CR>
nmap <space>s :Snippets<CR>
nmap <space>b :Buffers<CR>
nmap <space>w :Windows<CR>
nmap <space>f :Files<CR>
nmap <space>l :Lines<CR>

function! s:paste(txt)
    execute "normal a" . a:txt
endfunction
nnoremap <space>r :call fzf#run({
\   'source':
\     [@", @0, @-, @1, @2, @3],
\   'sink': function('<SID>paste'),
\   'options': '+m',
\   'up':    8
\ })<CR>

" ================= generic keymaps
nmap <Tab> gt
nmap <S-Tab> gT
map <C-_> <leader>c<space>
map <C-s> :w<CR>
nmap <leader>o :browse e .<CR>
nmap <leader>f :Grepper! -tool ag -open -switch<CR>
nmap <leader>l :lopen<CR>
nmap <leader>q :copen<CR>
nmap <F3> :Autoformat<CR>
nmap <F8> :TagbarToggle<CR>
nmap s <leader><leader>
vmap s S
noremap qr q"
noremap qe q
noremap qq :q<CR>
noremap qp @"

imap <C-f> <right>
imap <C-b> <left>
if has("nvim")
    tnoremap <Esc><Esc> <C-\><C-n>
    tnoremap <A-h> <C-\><C-n><C-w>h
    tnoremap <A-j> <C-\><C-n><C-w>j
    tnoremap <A-k> <C-\><C-n><C-w>k
    tnoremap <A-l> <C-\><C-n><C-w>l
    nnoremap <A-h> <C-w>h
    nnoremap <A-j> <C-w>j
    nnoremap <A-k> <C-w>k
    nnoremap <A-l> <C-w>l
endif

inoremap <A-b> <C-o>b
inoremap <A-f> <C-o>w
inoremap <C-a> <C-o>_
inoremap <C-e> <C-o>$
inoremap <C-k> <C-o>C

nmap >s >)
nmap <s <)
nmap >S >(
nmap <S <(

nmap ]v `>
nmap [v `<

nmap [h vat<esc>`<w
nmap ]h vat<esc>`>b
nmap [H vatat<esc>`<w
nmap ]H vatat<esc>`>b

"inoremap <expr> <Esc>      pumvisible() ? "\<C-e>" : "\<Esc>"
"inoremap <expr> <CR>       pumvisible() ? "\<C-y>" : "\<CR>"
